(function(){const n=document.createElement("link").relList;if(n&&n.supports&&n.supports("modulepreload"))return;for(const i of document.querySelectorAll('link[rel="modulepreload"]'))r(i);new MutationObserver(i=>{for(const s of i)if(s.type==="childList")for(const u of s.addedNodes)u.tagName==="LINK"&&u.rel==="modulepreload"&&r(u)}).observe(document,{childList:!0,subtree:!0});function t(i){const s={};return i.integrity&&(s.integrity=i.integrity),i.referrerPolicy&&(s.referrerPolicy=i.referrerPolicy),i.crossOrigin==="use-credentials"?s.credentials="include":i.crossOrigin==="anonymous"?s.credentials="omit":s.credentials="same-origin",s}function r(i){if(i.ep)return;i.ep=!0;const s=t(i);fetch(i.href,s)}})();var y=typeof globalThis<"u"?globalThis:typeof window<"u"?window:typeof global<"u"?global:typeof self<"u"?self:{},yt={exports:{}},B={exports:{}};/*!
 * Zdog v1.1.3
 * Round, flat, designer-friendly pseudo-3D engine
 * Licensed MIT
 * https://zzz.dog
 * Copyright 2020 Metafizzy
 */var tt;function S(){return tt||(tt=1,function(d){(function(n,t){d.exports?d.exports=t():n.Zdog=t()})(y,function(){var t={};t.TAU=Math.PI*2,t.extend=function(i,s){for(var u in s)i[u]=s[u];return i},t.lerp=function(i,s,u){return(s-i)*u+i},t.modulo=function(i,s){return(i%s+s)%s};var r={2:function(i){return i*i},3:function(i){return i*i*i},4:function(i){return i*i*i*i},5:function(i){return i*i*i*i*i}};return t.easeInOut=function(i,s){if(s==1)return i;i=Math.max(0,Math.min(1,i));var u=i<.5,e=u?i:1-i;e/=.5;var l=r[s]||r[2],a=l(e);return a/=2,u?a:1-a},t})}(B)),B.exports}var M={exports:{}},et;function wt(){return et||(et=1,function(d){(function(n,t){d.exports?d.exports=t():n.Zdog.CanvasRenderer=t()})(y,function(){var t={isCanvas:!0};return t.begin=function(r){r.beginPath()},t.move=function(r,i,s){r.moveTo(s.x,s.y)},t.line=function(r,i,s){r.lineTo(s.x,s.y)},t.bezier=function(r,i,s,u,e){r.bezierCurveTo(s.x,s.y,u.x,u.y,e.x,e.y)},t.closePath=function(r){r.closePath()},t.setPath=function(){},t.renderPath=function(r,i,s,u){this.begin(r,i),s.forEach(function(e){e.render(r,i,t)}),u&&this.closePath(r,i)},t.stroke=function(r,i,s,u,e){s&&(r.strokeStyle=u,r.lineWidth=e,r.stroke())},t.fill=function(r,i,s,u){s&&(r.fillStyle=u,r.fill())},t.end=function(){},t})}(M)),M.exports}var T={exports:{}},rt;function xt(){return rt||(rt=1,function(d){(function(n,t){d.exports?d.exports=t():n.Zdog.SvgRenderer=t()})(y,function(){var t={isSvg:!0},r=t.round=function(s){return Math.round(s*1e3)/1e3};function i(s){return r(s.x)+","+r(s.y)+" "}return t.begin=function(){},t.move=function(s,u,e){return"M"+i(e)},t.line=function(s,u,e){return"L"+i(e)},t.bezier=function(s,u,e,l,a){return"C"+i(e)+i(l)+i(a)},t.closePath=function(){return"Z"},t.setPath=function(s,u,e){u.setAttribute("d",e)},t.renderPath=function(s,u,e,l){var a="";e.forEach(function(o){a+=o.render(s,u,t)}),l&&(a+=this.closePath(s,u)),this.setPath(s,u,a)},t.stroke=function(s,u,e,l,a){e&&(u.setAttribute("stroke",l),u.setAttribute("stroke-width",a))},t.fill=function(s,u,e,l){var a=e?l:"none";u.setAttribute("fill",a)},t.end=function(s,u){s.appendChild(u)},t})}(T)),T.exports}var O={exports:{}},nt;function I(){return nt||(nt=1,function(d){(function(n,t){if(d.exports)d.exports=t(S());else{var r=n.Zdog;r.Vector=t(r)}})(y,function(t){function r(e){this.set(e)}var i=t.TAU;r.prototype.set=function(e){return this.x=e&&e.x||0,this.y=e&&e.y||0,this.z=e&&e.z||0,this},r.prototype.write=function(e){return e?(this.x=e.x!=null?e.x:this.x,this.y=e.y!=null?e.y:this.y,this.z=e.z!=null?e.z:this.z,this):this},r.prototype.rotate=function(e){if(e)return this.rotateZ(e.z),this.rotateY(e.y),this.rotateX(e.x),this},r.prototype.rotateZ=function(e){s(this,e,"x","y")},r.prototype.rotateX=function(e){s(this,e,"y","z")},r.prototype.rotateY=function(e){s(this,e,"x","z")};function s(e,l,a,o){if(!(!l||l%i===0)){var h=Math.cos(l),c=Math.sin(l),f=e[a],p=e[o];e[a]=f*h-p*c,e[o]=p*h+f*c}}r.prototype.isSame=function(e){return e?this.x===e.x&&this.y===e.y&&this.z===e.z:!1},r.prototype.add=function(e){return e?(this.x+=e.x||0,this.y+=e.y||0,this.z+=e.z||0,this):this},r.prototype.subtract=function(e){return e?(this.x-=e.x||0,this.y-=e.y||0,this.z-=e.z||0,this):this},r.prototype.multiply=function(e){return e==null?this:(typeof e=="number"?(this.x*=e,this.y*=e,this.z*=e):(this.x*=e.x!=null?e.x:1,this.y*=e.y!=null?e.y:1,this.z*=e.z!=null?e.z:1),this)},r.prototype.transform=function(e,l,a){return this.multiply(a),this.rotate(l),this.add(e),this},r.prototype.lerp=function(e,l){return this.x=t.lerp(this.x,e.x||0,l),this.y=t.lerp(this.y,e.y||0,l),this.z=t.lerp(this.z,e.z||0,l),this},r.prototype.magnitude=function(){var e=this.x*this.x+this.y*this.y+this.z*this.z;return u(e)};function u(e){return Math.abs(e-1)<1e-8?1:Math.sqrt(e)}return r.prototype.magnitude2d=function(){var e=this.x*this.x+this.y*this.y;return u(e)},r.prototype.copy=function(){return new r(this)},r})}(O)),O.exports}var N={exports:{}},it;function E(){return it||(it=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),I(),wt(),xt());else{var r=n.Zdog;r.Anchor=t(r,r.Vector,r.CanvasRenderer,r.SvgRenderer)}})(y,function(t,r,i,s){var u=t.TAU,e={x:1,y:1,z:1};function l(o){this.create(o||{})}l.prototype.create=function(o){this.children=[],t.extend(this,this.constructor.defaults),this.setOptions(o),this.translate=new r(o.translate),this.rotate=new r(o.rotate),this.scale=new r(e).multiply(this.scale),this.origin=new r,this.renderOrigin=new r,this.addTo&&this.addTo.addChild(this)},l.defaults={},l.optionKeys=Object.keys(l.defaults).concat(["rotate","translate","scale","addTo"]),l.prototype.setOptions=function(o){var h=this.constructor.optionKeys;for(var c in o)h.indexOf(c)!=-1&&(this[c]=o[c])},l.prototype.addChild=function(o){this.children.indexOf(o)==-1&&(o.remove(),o.addTo=this,this.children.push(o))},l.prototype.removeChild=function(o){var h=this.children.indexOf(o);h!=-1&&this.children.splice(h,1)},l.prototype.remove=function(){this.addTo&&this.addTo.removeChild(this)},l.prototype.update=function(){this.reset(),this.children.forEach(function(o){o.update()}),this.transform(this.translate,this.rotate,this.scale)},l.prototype.reset=function(){this.renderOrigin.set(this.origin)},l.prototype.transform=function(o,h,c){this.renderOrigin.transform(o,h,c),this.children.forEach(function(f){f.transform(o,h,c)})},l.prototype.updateGraph=function(){this.update(),this.updateFlatGraph(),this.flatGraph.forEach(function(o){o.updateSortValue()}),this.flatGraph.sort(l.shapeSorter)},l.shapeSorter=function(o,h){return o.sortValue-h.sortValue},Object.defineProperty(l.prototype,"flatGraph",{get:function(){return this._flatGraph||this.updateFlatGraph(),this._flatGraph},set:function(o){this._flatGraph=o}}),l.prototype.updateFlatGraph=function(){this.flatGraph=this.getFlatGraph()},l.prototype.getFlatGraph=function(){var o=[this];return this.addChildFlatGraph(o)},l.prototype.addChildFlatGraph=function(o){return this.children.forEach(function(h){var c=h.getFlatGraph();Array.prototype.push.apply(o,c)}),o},l.prototype.updateSortValue=function(){this.sortValue=this.renderOrigin.z},l.prototype.render=function(){},l.prototype.renderGraphCanvas=function(o){if(!o)throw new Error("ctx is "+o+". Canvas context required for render. Check .renderGraphCanvas( ctx ).");this.flatGraph.forEach(function(h){h.render(o,i)})},l.prototype.renderGraphSvg=function(o){if(!o)throw new Error("svg is "+o+". SVG required for render. Check .renderGraphSvg( svg ).");this.flatGraph.forEach(function(h){h.render(o,s)})},l.prototype.copy=function(o){var h={},c=this.constructor.optionKeys;c.forEach(function(p){h[p]=this[p]},this),t.extend(h,o);var f=this.constructor;return new f(h)},l.prototype.copyGraph=function(o){var h=this.copy(o);return this.children.forEach(function(c){c.copyGraph({addTo:h})}),h},l.prototype.normalizeRotate=function(){this.rotate.x=t.modulo(this.rotate.x,u),this.rotate.y=t.modulo(this.rotate.y,u),this.rotate.z=t.modulo(this.rotate.z,u)};function a(o){return function(h){function c(f){this.create(f||{})}return c.prototype=Object.create(o.prototype),c.prototype.constructor=c,c.defaults=t.extend({},o.defaults),t.extend(c.defaults,h),c.optionKeys=o.optionKeys.slice(0),Object.keys(c.defaults).forEach(function(f){!c.optionKeys.indexOf(f)!=1&&c.optionKeys.push(f)}),c.subclass=a(c),c}}return l.subclass=a(l),l})}(N)),N.exports}var j={exports:{}},ot;function bt(){return ot||(ot=1,function(d){(function(n,t){d.exports?d.exports=t():n.Zdog.Dragger=t()})(y,function(){var t=typeof window<"u",r="mousedown",i="mousemove",s="mouseup";t&&(window.PointerEvent?(r="pointerdown",i="pointermove",s="pointerup"):"ontouchstart"in window&&(r="touchstart",i="touchmove",s="touchend"));function u(){}function e(l){this.create(l||{})}return e.prototype.create=function(l){this.onDragStart=l.onDragStart||u,this.onDragMove=l.onDragMove||u,this.onDragEnd=l.onDragEnd||u,this.bindDrag(l.startElement)},e.prototype.bindDrag=function(l){l=this.getQueryElement(l),l&&(l.style.touchAction="none",l.addEventListener(r,this))},e.prototype.getQueryElement=function(l){return typeof l=="string"&&(l=document.querySelector(l)),l},e.prototype.handleEvent=function(l){var a=this["on"+l.type];a&&a.call(this,l)},e.prototype.onmousedown=e.prototype.onpointerdown=function(l){this.dragStart(l,l)},e.prototype.ontouchstart=function(l){this.dragStart(l,l.changedTouches[0])},e.prototype.dragStart=function(l,a){l.preventDefault(),this.dragStartX=a.pageX,this.dragStartY=a.pageY,t&&(window.addEventListener(i,this),window.addEventListener(s,this)),this.onDragStart(a)},e.prototype.ontouchmove=function(l){this.dragMove(l,l.changedTouches[0])},e.prototype.onmousemove=e.prototype.onpointermove=function(l){this.dragMove(l,l)},e.prototype.dragMove=function(l,a){l.preventDefault();var o=a.pageX-this.dragStartX,h=a.pageY-this.dragStartY;this.onDragMove(a,o,h)},e.prototype.onmouseup=e.prototype.onpointerup=e.prototype.ontouchend=e.prototype.dragEnd=function(){window.removeEventListener(i,this),window.removeEventListener(s,this),this.onDragEnd()},e})}(j)),j.exports}var q={exports:{}},at;function It(){return at||(at=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),E(),bt());else{var r=n.Zdog;r.Illustration=t(r,r.Anchor,r.Dragger)}})(y,function(t,r,i){function s(){}var u=t.TAU,e=r.subclass({element:void 0,centered:!0,zoom:1,dragRotate:!1,resize:!1,onPrerender:s,onDragStart:s,onDragMove:s,onDragEnd:s,onResize:s});t.extend(e.prototype,i.prototype),e.prototype.create=function(a){r.prototype.create.call(this,a),i.prototype.create.call(this,a),this.setElement(this.element),this.setDragRotate(this.dragRotate),this.setResize(this.resize)},e.prototype.setElement=function(a){if(a=this.getQueryElement(a),!a)throw new Error("Zdog.Illustration element required. Set to "+a);var o=a.nodeName.toLowerCase();o=="canvas"?this.setCanvas(a):o=="svg"&&this.setSvg(a)},e.prototype.setSize=function(a,o){a=Math.round(a),o=Math.round(o),this.isCanvas?this.setSizeCanvas(a,o):this.isSvg&&this.setSizeSvg(a,o)},e.prototype.setResize=function(a){this.resize=a,this.resizeListener||(this.resizeListener=this.onWindowResize.bind(this)),a?(window.addEventListener("resize",this.resizeListener),this.onWindowResize()):window.removeEventListener("resize",this.resizeListener)},e.prototype.onWindowResize=function(){this.setMeasuredSize(),this.onResize(this.width,this.height)},e.prototype.setMeasuredSize=function(){var a,o,h=this.resize=="fullscreen";if(h)a=window.innerWidth,o=window.innerHeight;else{var c=this.element.getBoundingClientRect();a=c.width,o=c.height}this.setSize(a,o)},e.prototype.renderGraph=function(a){this.isCanvas?this.renderGraphCanvas(a):this.isSvg&&this.renderGraphSvg(a)},e.prototype.updateRenderGraph=function(a){this.updateGraph(),this.renderGraph(a)},e.prototype.setCanvas=function(a){this.element=a,this.isCanvas=!0,this.ctx=this.element.getContext("2d"),this.setSizeCanvas(a.width,a.height)},e.prototype.setSizeCanvas=function(a,o){this.width=a,this.height=o;var h=this.pixelRatio=window.devicePixelRatio||1;this.element.width=this.canvasWidth=a*h,this.element.height=this.canvasHeight=o*h;var c=h>1&&!this.resize;c&&(this.element.style.width=a+"px",this.element.style.height=o+"px")},e.prototype.renderGraphCanvas=function(a){a=a||this,this.prerenderCanvas(),r.prototype.renderGraphCanvas.call(a,this.ctx),this.postrenderCanvas()},e.prototype.prerenderCanvas=function(){var a=this.ctx;if(a.lineCap="round",a.lineJoin="round",a.clearRect(0,0,this.canvasWidth,this.canvasHeight),a.save(),this.centered){var o=this.width/2*this.pixelRatio,h=this.height/2*this.pixelRatio;a.translate(o,h)}var c=this.pixelRatio*this.zoom;a.scale(c,c),this.onPrerender(a)},e.prototype.postrenderCanvas=function(){this.ctx.restore()},e.prototype.setSvg=function(a){this.element=a,this.isSvg=!0,this.pixelRatio=1;var o=a.getAttribute("width"),h=a.getAttribute("height");this.setSizeSvg(o,h)},e.prototype.setSizeSvg=function(a,o){this.width=a,this.height=o;var h=a/this.zoom,c=o/this.zoom,f=this.centered?-h/2:0,p=this.centered?-c/2:0;this.element.setAttribute("viewBox",f+" "+p+" "+h+" "+c),this.resize?(this.element.removeAttribute("width"),this.element.removeAttribute("height")):(this.element.setAttribute("width",a),this.element.setAttribute("height",o))},e.prototype.renderGraphSvg=function(a){a=a||this,l(this.element),this.onPrerender(this.element),r.prototype.renderGraphSvg.call(a,this.element)};function l(a){for(;a.firstChild;)a.removeChild(a.firstChild)}return e.prototype.setDragRotate=function(a){if(a)a===!0&&(a=this);else return;this.dragRotate=a,this.bindDrag(this.element)},e.prototype.dragStart=function(){this.dragStartRX=this.dragRotate.rotate.x,this.dragStartRY=this.dragRotate.rotate.y,i.prototype.dragStart.apply(this,arguments)},e.prototype.dragMove=function(a,o){var h=o.pageX-this.dragStartX,c=o.pageY-this.dragStartY,f=Math.min(this.width,this.height),p=h/f*u,m=c/f*u;this.dragRotate.rotate.x=this.dragStartRX-m,this.dragRotate.rotate.y=this.dragStartRY-p,i.prototype.dragMove.apply(this,arguments)},e})}(q)),q.exports}var V={exports:{}},st;function L(){return st||(st=1,function(d){(function(n,t){if(d.exports)d.exports=t(I());else{var r=n.Zdog;r.PathCommand=t(r.Vector)}})(y,function(t){function r(e,l,a){this.method=e,this.points=l.map(i),this.renderPoints=l.map(s),this.previousPoint=a,this.endRenderPoint=this.renderPoints[this.renderPoints.length-1],e=="arc"&&(this.controlPoints=[new t,new t])}function i(e){return e instanceof t?e:new t(e)}function s(e){return new t(e)}r.prototype.reset=function(){var e=this.points;this.renderPoints.forEach(function(l,a){var o=e[a];l.set(o)})},r.prototype.transform=function(e,l,a){this.renderPoints.forEach(function(o){o.transform(e,l,a)})},r.prototype.render=function(e,l,a){return this[this.method](e,l,a)},r.prototype.move=function(e,l,a){return a.move(e,l,this.renderPoints[0])},r.prototype.line=function(e,l,a){return a.line(e,l,this.renderPoints[0])},r.prototype.bezier=function(e,l,a){var o=this.renderPoints[0],h=this.renderPoints[1],c=this.renderPoints[2];return a.bezier(e,l,o,h,c)};var u=9/16;return r.prototype.arc=function(e,l,a){var o=this.previousPoint,h=this.renderPoints[0],c=this.renderPoints[1],f=this.controlPoints[0],p=this.controlPoints[1];return f.set(o).lerp(h,u),p.set(c).lerp(h,u),a.bezier(e,l,f,p,c)},r})}(V)),V.exports}var X={exports:{}},lt;function C(){return lt||(lt=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),I(),L(),E());else{var r=n.Zdog;r.Shape=t(r,r.Vector,r.PathCommand,r.Anchor)}})(y,function(t,r,i,s){var u=s.subclass({stroke:1,fill:!1,color:"#333",closed:!0,visible:!0,path:[{}],front:{z:1},backface:!0});u.prototype.create=function(o){s.prototype.create.call(this,o),this.updatePath(),this.front=new r(o.front||this.front),this.renderFront=new r(this.front),this.renderNormal=new r};var e=["move","line","bezier","arc"];u.prototype.updatePath=function(){this.setPath(),this.updatePathCommands()},u.prototype.setPath=function(){},u.prototype.updatePathCommands=function(){var o;this.pathCommands=this.path.map(function(h,c){var f=Object.keys(h),p=f[0],m=h[p],v=f.length==1&&e.indexOf(p)!=-1;v||(p="line",m=h);var g=p=="line"||p=="move",x=Array.isArray(m);g&&!x&&(m=[m]),p=c===0?"move":p;var b=new i(p,m,o);return o=b.endRenderPoint,b})},u.prototype.reset=function(){this.renderOrigin.set(this.origin),this.renderFront.set(this.front),this.pathCommands.forEach(function(o){o.reset()})},u.prototype.transform=function(o,h,c){this.renderOrigin.transform(o,h,c),this.renderFront.transform(o,h,c),this.renderNormal.set(this.renderOrigin).subtract(this.renderFront),this.pathCommands.forEach(function(f){f.transform(o,h,c)}),this.children.forEach(function(f){f.transform(o,h,c)})},u.prototype.updateSortValue=function(){var o=this.pathCommands.length,h=this.pathCommands[0].endRenderPoint,c=this.pathCommands[o-1].endRenderPoint,f=o>2&&h.isSame(c);f&&(o-=1);for(var p=0,m=0;m<o;m++)p+=this.pathCommands[m].endRenderPoint.z;this.sortValue=p/o},u.prototype.render=function(o,h){var c=this.pathCommands.length;if(!(!this.visible||!c)&&(this.isFacingBack=this.renderNormal.z>0,!(!this.backface&&this.isFacingBack))){if(!h)throw new Error("Zdog renderer required. Set to "+h);var f=c==1;h.isCanvas&&f?this.renderCanvasDot(o,h):this.renderPath(o,h)}};var l=t.TAU;u.prototype.renderCanvasDot=function(o){var h=this.getLineWidth();if(h){o.fillStyle=this.getRenderColor();var c=this.pathCommands[0].endRenderPoint;o.beginPath();var f=h/2;o.arc(c.x,c.y,f,0,l),o.fill()}},u.prototype.getLineWidth=function(){return this.stroke?this.stroke==!0?1:this.stroke:0},u.prototype.getRenderColor=function(){var o=typeof this.backface=="string"&&this.isFacingBack,h=o?this.backface:this.color;return h},u.prototype.renderPath=function(o,h){var c=this.getRenderElement(o,h),f=this.pathCommands.length==2&&this.pathCommands[1].method=="line",p=!f&&this.closed,m=this.getRenderColor();h.renderPath(o,c,this.pathCommands,p),h.stroke(o,c,this.stroke,m,this.getLineWidth()),h.fill(o,c,this.fill,m),h.end(o,c)};var a="http://www.w3.org/2000/svg";return u.prototype.getRenderElement=function(o,h){if(h.isSvg)return this.svgElement||(this.svgElement=document.createElementNS(a,"path"),this.svgElement.setAttribute("stroke-linecap","round"),this.svgElement.setAttribute("stroke-linejoin","round")),this.svgElement},u})}(X)),X.exports}var Y={exports:{}},dt;function St(){return dt||(dt=1,function(d){(function(n,t){if(d.exports)d.exports=t(E());else{var r=n.Zdog;r.Group=t(r.Anchor)}})(y,function(t){var r=t.subclass({updateSort:!1,visible:!0});return r.prototype.updateSortValue=function(){var i=0;this.flatGraph.forEach(function(s){s.updateSortValue(),i+=s.sortValue}),this.sortValue=i/this.flatGraph.length,this.updateSort&&this.flatGraph.sort(t.shapeSorter)},r.prototype.render=function(i,s){this.visible&&this.flatGraph.forEach(function(u){u.render(i,s)})},r.prototype.updateFlatGraph=function(){var i=[];this.flatGraph=this.addChildFlatGraph(i)},r.prototype.getFlatGraph=function(){return[this]},r})}(Y)),Y.exports}var U={exports:{}},ut;function Et(){return ut||(ut=1,function(d){(function(n,t){if(d.exports)d.exports=t(C());else{var r=n.Zdog;r.Rect=t(r.Shape)}})(y,function(t){var r=t.subclass({width:1,height:1});return r.prototype.setPath=function(){var i=this.width/2,s=this.height/2;this.path=[{x:-i,y:-s},{x:i,y:-s},{x:i,y:s},{x:-i,y:s}]},r})}(U)),U.exports}var W={exports:{}},ht;function Pt(){return ht||(ht=1,function(d){(function(n,t){if(d.exports)d.exports=t(C());else{var r=n.Zdog;r.RoundedRect=t(r.Shape)}})(y,function(t){var r=t.subclass({width:1,height:1,cornerRadius:.25,closed:!1});return r.prototype.setPath=function(){var i=this.width/2,s=this.height/2,u=Math.min(i,s),e=Math.min(this.cornerRadius,u),l=i-e,a=s-e,o=[{x:l,y:-s},{arc:[{x:i,y:-s},{x:i,y:-a}]}];a&&o.push({x:i,y:a}),o.push({arc:[{x:i,y:s},{x:l,y:s}]}),l&&o.push({x:-l,y:s}),o.push({arc:[{x:-i,y:s},{x:-i,y:a}]}),a&&o.push({x:-i,y:-a}),o.push({arc:[{x:-i,y:-s},{x:-l,y:-s}]}),l&&o.push({x:l,y:-s}),this.path=o},r})}(W)),W.exports}var K={exports:{}},ct;function G(){return ct||(ct=1,function(d){(function(n,t){if(d.exports)d.exports=t(C());else{var r=n.Zdog;r.Ellipse=t(r.Shape)}})(y,function(t){var r=t.subclass({diameter:1,width:void 0,height:void 0,quarters:4,closed:!1});return r.prototype.setPath=function(){var i=this.width!=null?this.width:this.diameter,s=this.height!=null?this.height:this.diameter,u=i/2,e=s/2;this.path=[{x:0,y:-e},{arc:[{x:u,y:-e},{x:u,y:0}]}],this.quarters>1&&this.path.push({arc:[{x:u,y:e},{x:0,y:e}]}),this.quarters>2&&this.path.push({arc:[{x:-u,y:e},{x:-u,y:0}]}),this.quarters>3&&this.path.push({arc:[{x:-u,y:-e},{x:0,y:-e}]})},r})}(K)),K.exports}var H={exports:{}},pt;function Rt(){return pt||(pt=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),C());else{var r=n.Zdog;r.Polygon=t(r,r.Shape)}})(y,function(t,r){var i=r.subclass({sides:3,radius:.5}),s=t.TAU;return i.prototype.setPath=function(){this.path=[];for(var u=0;u<this.sides;u++){var e=u/this.sides*s-s/4,l=Math.cos(e)*this.radius,a=Math.sin(e)*this.radius;this.path.push({x:l,y:a})}},i})}(H)),H.exports}var _={exports:{}},ft;function At(){return ft||(ft=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),I(),E(),G());else{var r=n.Zdog;r.Hemisphere=t(r,r.Vector,r.Anchor,r.Ellipse)}})(y,function(t,r,i,s){var u=s.subclass({fill:!0}),e=t.TAU;u.prototype.create=function(){s.prototype.create.apply(this,arguments),this.apex=new i({addTo:this,translate:{z:this.diameter/2}}),this.renderCentroid=new r},u.prototype.updateSortValue=function(){this.renderCentroid.set(this.renderOrigin).lerp(this.apex.renderOrigin,3/8),this.sortValue=this.renderCentroid.z},u.prototype.render=function(a,o){this.renderDome(a,o),s.prototype.render.apply(this,arguments)},u.prototype.renderDome=function(a,o){if(this.visible){var h=this.getDomeRenderElement(a,o),c=Math.atan2(this.renderNormal.y,this.renderNormal.x),f=this.diameter/2*this.renderNormal.magnitude(),p=this.renderOrigin.x,m=this.renderOrigin.y;if(o.isCanvas){var v=c+e/4,g=c-e/4;a.beginPath(),a.arc(p,m,f,v,g)}else o.isSvg&&(c=(c-e/4)/e*360,this.domeSvgElement.setAttribute("d","M "+-f+",0 A "+f+","+f+" 0 0 1 "+f+",0"),this.domeSvgElement.setAttribute("transform","translate("+p+","+m+" ) rotate("+c+")"));o.stroke(a,h,this.stroke,this.color,this.getLineWidth()),o.fill(a,h,this.fill,this.color),o.end(a,h)}};var l="http://www.w3.org/2000/svg";return u.prototype.getDomeRenderElement=function(a,o){if(o.isSvg)return this.domeSvgElement||(this.domeSvgElement=document.createElementNS(l,"path"),this.domeSvgElement.setAttribute("stroke-linecap","round"),this.domeSvgElement.setAttribute("stroke-linejoin","round")),this.domeSvgElement},u})}(_)),_.exports}var J={exports:{}},mt;function zt(){return mt||(mt=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),L(),C(),St(),G());else{var r=n.Zdog;r.Cylinder=t(r,r.PathCommand,r.Shape,r.Group,r.Ellipse)}})(y,function(t,r,i,s,u){function e(){}var l=s.subclass({color:"#333",updateSort:!0});l.prototype.create=function(){s.prototype.create.apply(this,arguments),this.pathCommands=[new r("move",[{}]),new r("line",[{}])]},l.prototype.render=function(p,m){this.renderCylinderSurface(p,m),s.prototype.render.apply(this,arguments)},l.prototype.renderCylinderSurface=function(p,m){if(this.visible){var v=this.getRenderElement(p,m),g=this.frontBase,x=this.rearBase,b=g.renderNormal.magnitude(),F=g.diameter*b+g.getLineWidth();this.pathCommands[0].renderPoints[0].set(g.renderOrigin),this.pathCommands[1].renderPoints[0].set(x.renderOrigin),m.isCanvas&&(p.lineCap="butt"),m.renderPath(p,v,this.pathCommands),m.stroke(p,v,!0,this.color,F),m.end(p,v),m.isCanvas&&(p.lineCap="round")}};var a="http://www.w3.org/2000/svg";l.prototype.getRenderElement=function(p,m){if(m.isSvg)return this.svgElement||(this.svgElement=document.createElementNS(a,"path")),this.svgElement},l.prototype.copyGraph=e;var o=u.subclass();o.prototype.copyGraph=e;var h=i.subclass({diameter:1,length:1,frontFace:void 0,fill:!0}),c=t.TAU;h.prototype.create=function(){i.prototype.create.apply(this,arguments),this.group=new l({addTo:this,color:this.color,visible:this.visible});var p=this.length/2,m=this.backface||!0;this.frontBase=this.group.frontBase=new u({addTo:this.group,diameter:this.diameter,translate:{z:p},rotate:{y:c/2},color:this.color,stroke:this.stroke,fill:this.fill,backface:this.frontFace||m,visible:this.visible}),this.rearBase=this.group.rearBase=this.frontBase.copy({translate:{z:-p},rotate:{y:0},backface:m})},h.prototype.render=function(){};var f=["stroke","fill","color","visible"];return f.forEach(function(p){var m="_"+p;Object.defineProperty(h.prototype,p,{get:function(){return this[m]},set:function(v){this[m]=v,this.frontBase&&(this.frontBase[p]=v,this.rearBase[p]=v,this.group[p]=v)}})}),h})}(J)),J.exports}var Q={exports:{}},vt;function Dt(){return vt||(vt=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),I(),L(),E(),G());else{var r=n.Zdog;r.Cone=t(r,r.Vector,r.PathCommand,r.Anchor,r.Ellipse)}})(y,function(t,r,i,s,u){var e=u.subclass({length:1,fill:!0}),l=t.TAU;e.prototype.create=function(){u.prototype.create.apply(this,arguments),this.apex=new s({addTo:this,translate:{z:this.length}}),this.renderApex=new r,this.renderCentroid=new r,this.tangentA=new r,this.tangentB=new r,this.surfacePathCommands=[new i("move",[{}]),new i("line",[{}]),new i("line",[{}])]},e.prototype.updateSortValue=function(){this.renderCentroid.set(this.renderOrigin).lerp(this.apex.renderOrigin,1/3),this.sortValue=this.renderCentroid.z},e.prototype.render=function(o,h){this.renderConeSurface(o,h),u.prototype.render.apply(this,arguments)},e.prototype.renderConeSurface=function(o,h){if(this.visible){this.renderApex.set(this.apex.renderOrigin).subtract(this.renderOrigin);var c=this.renderNormal.magnitude(),f=this.renderApex.magnitude2d(),p=this.renderNormal.magnitude2d(),m=Math.acos(p/c),v=Math.sin(m),g=this.diameter/2*c,x=g*v<f;if(x){var b=Math.atan2(this.renderNormal.y,this.renderNormal.x)+l/2,F=f/v,Z=Math.acos(g/F),P=this.tangentA,R=this.tangentB;P.x=Math.cos(Z)*g*v,P.y=Math.sin(Z)*g,R.set(this.tangentA),R.y*=-1,P.rotateZ(b),R.rotateZ(b),P.add(this.renderOrigin),R.add(this.renderOrigin),this.setSurfaceRenderPoint(0,P),this.setSurfaceRenderPoint(1,this.apex.renderOrigin),this.setSurfaceRenderPoint(2,R);var z=this.getSurfaceRenderElement(o,h);h.renderPath(o,z,this.surfacePathCommands),h.stroke(o,z,this.stroke,this.color,this.getLineWidth()),h.fill(o,z,this.fill,this.color),h.end(o,z)}}};var a="http://www.w3.org/2000/svg";return e.prototype.getSurfaceRenderElement=function(o,h){if(h.isSvg)return this.surfaceSvgElement||(this.surfaceSvgElement=document.createElementNS(a,"path"),this.surfaceSvgElement.setAttribute("stroke-linecap","round"),this.surfaceSvgElement.setAttribute("stroke-linejoin","round")),this.surfaceSvgElement},e.prototype.setSurfaceRenderPoint=function(o,h){var c=this.surfacePathCommands[o].renderPoints[0];c.set(h)},e})}(Q)),Q.exports}var $={exports:{}},gt;function Lt(){return gt||(gt=1,function(d){(function(n,t){if(d.exports)d.exports=t(S(),E(),C(),Et());else{var r=n.Zdog;r.Box=t(r,r.Anchor,r.Shape,r.Rect)}})(y,function(t,r,i,s){var u=s.subclass();u.prototype.copyGraph=function(){};var e=t.TAU,l=["frontFace","rearFace","leftFace","rightFace","topFace","bottomFace"],a=t.extend({},i.defaults);delete a.path,l.forEach(function(c){a[c]=!0}),t.extend(a,{width:1,height:1,depth:1,fill:!0});var o=r.subclass(a);o.prototype.create=function(c){r.prototype.create.call(this,c),this.updatePath(),this.fill=this.fill},o.prototype.updatePath=function(){l.forEach(function(c){this[c]=this[c]},this)},l.forEach(function(c){var f="_"+c;Object.defineProperty(o.prototype,c,{get:function(){return this[f]},set:function(p){this[f]=p,this.setFace(c,p)}})}),o.prototype.setFace=function(c,f){var p=c+"Rect",m=this[p];if(!f){this.removeChild(m);return}var v=this.getFaceOptions(c);v.color=typeof f=="string"?f:this.color,m?m.setOptions(v):m=this[p]=new u(v),m.updatePath(),this.addChild(m)},o.prototype.getFaceOptions=function(c){return{frontFace:{width:this.width,height:this.height,translate:{z:this.depth/2}},rearFace:{width:this.width,height:this.height,translate:{z:-this.depth/2},rotate:{y:e/2}},leftFace:{width:this.depth,height:this.height,translate:{x:-this.width/2},rotate:{y:-e/4}},rightFace:{width:this.depth,height:this.height,translate:{x:this.width/2},rotate:{y:e/4}},topFace:{width:this.width,height:this.depth,translate:{y:-this.height/2},rotate:{x:-e/4}},bottomFace:{width:this.width,height:this.depth,translate:{y:this.height/2},rotate:{x:e/4}}}[c]};var h=["color","stroke","fill","backface","front","visible"];return h.forEach(function(c){var f="_"+c;Object.defineProperty(o.prototype,c,{get:function(){return this[f]},set:function(p){this[f]=p,l.forEach(function(m){var v=this[m+"Rect"],g=typeof this[m]=="string",x=c=="color"&&g;v&&!x&&(v[c]=p)},this)}})}),o})}($)),$.exports}(function(d){(function(n,t){d.exports&&(d.exports=t(S(),wt(),xt(),I(),E(),bt(),It(),L(),C(),St(),Et(),Pt(),G(),Rt(),At(),zt(),Dt(),Lt()))})(y,function(t,r,i,s,u,e,l,a,o,h,c,f,p,m,v,g,x,b){return t.CanvasRenderer=r,t.SvgRenderer=i,t.Vector=s,t.Anchor=u,t.Dragger=e,t.Illustration=l,t.PathCommand=a,t.Shape=o,t.Group=h,t.Rect=c,t.RoundedRect=f,t.Ellipse=p,t.Polygon=m,t.Hemisphere=v,t.Cylinder=g,t.Cone=x,t.Box=b,t})})(yt);var w=yt.exports;class Gt{constructor(n=null){this.commands=[].concat(n||[]),this.index=0}drawAllShapes(){for(let n=0;n<this.commands.length;n++)n>0?this.commands[n].addShape({x:+this.commands[n-1].goalx,y:+this.commands[n-1].goaly,z:+this.commands[n-1].goalz}):this.commands[n].addShape({x:0,y:0,z:0})}append(n=null){n=n||[],Array.isArray(n)||(n=[n]);for(var t=0;t<n.length;t++)t==0?this.commands.length>0?n[t].addShape({x:+this.commands[this.commands.length-1].goalx,y:+this.commands[this.commands.length-1].goaly,z:+this.commands[this.commands.length-1].goalz}):n[t].addShape({x:0,y:0,z:0}):n[t].addShape({x:+n[t-1].goalx,y:+n[t-1].goaly,z:+n[t-1].goalz}),n[t].addListElement();this.commands=this.commands.concat(n)}insert(n,t=null){this.commands.splice(this.commands.indexOf(n)+1,0,t||[])}remove(n){const t=this.commands.indexOf(n);n.removeSelection(),n.removeShape(),this.commands[t+1]&&(this.commands[t+1].removeShape(),t>0?this.commands[t+1].addShape({x:this.commands[t-1].goalx,y:this.commands[t-1].goaly,z:this.commands[t-1].goalz}):this.commands[t+1].addShape({x:0,y:0,z:0})),this.commands.splice(t,1)}currentCoordinates(){const n=this.commands.length;let t={x:0,y:0,z:0,e:0};for(let r=n;r>0;r--)t.x=+this.commands[n-1].goalx.value,t.y=+this.commands[n-1].goaly.value,t.z=+this.commands[n-1].goalz.value,t.e=+this.commands[n-1].flow.value;return t}selectCommand(n){const t=this.commands[n];return t.addSelection(),t}deselectCommand(n){const t=this.commands[n];return t.removeSelection(),t}next(){return this.index<this.commands.length?{value:this.commands[this.index++],done:!1}:(this.index=0,{done:!0})}[Symbol.iterator](){return this.index=0,this}toString(){this.commands.reduce((n,t)=>n+`
`+t,"")}}class A{constructor(n){this.value=n}valueOf(){return this.value!==null?this.value.toFixed(8):0}getValue(){return this.value}setValue(n){this.value=n}}class kt extends A{toString(){var n="";return this.value!==null&&(n="X"+this.value.toFixed(8)),n}getDescription(){return"X"}}class Ft extends A{toString(){var n="";return this.value!==null&&(n="Y"+this.value.toFixed(8)),n}getDescription(){return"Y"}}class Bt extends A{toString(){var n="";return this.value!==null&&(n="Z"+this.value.toFixed(8)),n}getDescription(){return"Z"}}class Mt extends A{toString(){var n="";return this.value!==null&&(n="F"+this.value),n}getDescription(){return"Speed"}}class Tt extends A{toString(){var n="";return this.value!==null&&(n="E"+this.value.toFixed(8)),n}getDescription(){return"Flow"}}class Ot{constructor(){}toString(){return""}addSelection(){this.selected=!0}removeSelection(){this.selected=!1}removeShape(){this.shape&&(window.illo.removeChild(this.shape),delete this.shape)}getTimeDuration(){return 20}}class D extends Ot{constructor(n,t,r,i,s){super(),this.flow=new Tt(n),this.speed=new Mt(t),this.goalx=new kt(r),this.goaly=new Ft(i),this.goalz=new Bt(s)}addListElement(){this.listElement=document.createElement("li"),this.listElement.textContent=this.getDescription(),this.listElement.command=this,document.getElementById("list-commands").appendChild(this.listElement)}addShape(n){this.shape=new w.Shape({addTo:window.illo,path:[{y:n.x,x:n.y,z:n.z},{y:+this.goalx,x:+this.goaly,z:+this.goalz}],stroke:.9,color:"black"})}addSelection(){if(super.addSelection(),!this.arrowShape){this.listElement.classList.add("selected"),this.shape.color="red",this.shape.stroke=1.2;var n=new w.Vector({x:this.shape.path[1].x-this.shape.path[0].x,y:this.shape.path[1].y-this.shape.path[0].y,z:this.shape.path[1].z-this.shape.path[0].z});n.multiply(1/n.magnitude());var t=new w.Vector({x:-Math.atan2(n.y,n.z),y:-Math.atan2(n.x,Math.hypot(n.y,n.z)),z:0});this.arrowShape=new w.Cone({addTo:window.illo,diameter:1,length:3,stroke:!1,color:"red",backface:"black",translate:{x:+this.goaly,y:+this.goalx,z:+this.goalz},rotate:t})}}removeSelection(){super.removeSelection(),this.listElement.classList.remove("selected"),this.shape.color="black",this.shape.stroke=.9,window.illo.removeChild(this.arrowShape),delete this.arrowShape}getLength(){const n=new w.Vector({x:this.shape.path[1].x-this.shape.path[0].x,y:this.shape.path[1].y-this.shape.path[0].y,z:this.shape.path[1].z-this.shape.path[0].z});return Math.hypot(n.x,n.y,n.z)}getTimeDuration(){return console.log("Speed: "+this.speed.value+", Length:"+this.getLength()),25e3/this.speed.value*this.getLength()}toString(){return["G1",this.flow,this.speed,this.goalx,this.goaly,this.goalz].join(" ")}getDescription(){return"Move "+Math.round(this.flow).toString()+": ["+Math.round(this.goalx).toString().padStart(3,0)+", "+Math.round(this.goaly).toString().padStart(3,0)+", "+Math.round(this.goalz).toString().padStart(3,0)+"]"}}class k{constructor(){this.model=new Gt,this.addLine(0,2e3,0,0,0)}renderAll(){window.illo.children=[],this.refreshGridLines(),this.model.drawAllShapes()}refreshGridLines(){Number.isInteger(parseInt(localStorage.getItem("settingsGridSize"),10))||localStorage.setItem("settingsGridSize","10");let n=parseInt(localStorage.getItem("settingsGridSize"),10);if(window.illoGridGroup&&window.illo.removeChild(window.illoGridGroup),localStorage.getItem("settingsGridDisplay")=="off")return;window.illoGridGroup=new w.Group({addTo:window.illo});let t=250,r=parseInt(t/n)*n;for(var i=-r;i<r;i+=n){var s="#bbbbbb";i==0&&(s="#111111"),new w.Shape({addTo:window.illoGridGroup,path:[{y:-t,x:i,z:0},{y:t,x:i,z:0}],stroke:.5,color:s}),new w.Shape({addTo:window.illoGridGroup,path:[{y:i,x:-t,z:0},{y:i,x:t,z:0}],stroke:.5,color:s})}}addLine(n,t,r,i,s){n<this.model.currentCoordinates().e&&(n=this.model.currentCoordinates().e);var u=new D(n,t,r,i,s);this.model.append(u)}addArc(n,t,r,i,s,u){const e=this.model.currentCoordinates(),l=Math.min(r,50),a=(n-e.e)/l,o=90;for(var h=0;h<=l;h++){var c=h/l*o*.017453292519943295,f=r*(1-Math.cos(c)),p=r*Math.sin(c);if(u=="1"&&s=="2"||u=="2"&&s=="1"){var m=f;f=p,p=m}switch(i){default:var v=e.x+f,g=e.y;break;case"2":var v=e.x,g=e.y+f;break;case"3":var v=e.x-f,g=e.y;break;case"4":var v=e.x,g=e.y-f;break}switch(s){default:var x=e.z+p;break;case"2":var x=e.z-p;break;case"3":var x=e.z;switch(i){default:var v=e.x+p,g=e.y+f;break;case"2":var v=e.x-p,g=e.y+f;break;case"3":var v=e.x-p,g=e.y-f;break;case"4":var v=e.x+p,g=e.y-f;break}break}var b=new D(e.e+a*h,t,v,g,x);this.model.append(b)}}playbackDelay(){return new Promise(n=>{setTimeout(n,250)})}async drawPlaybackStart(){console.log("start simulation"),document.getElementById("playViewButton").lastChild.nodeValue=" Stop",window.stopPlayback=!1;for(var n=0;this.model.commands[n]&&(this.model.selectCommand(n),await this.playbackDelay(),this.model.deselectCommand(n),n++,!window.stopPlayback););console.log("stop simulation"),document.getElementById("playViewButton").lastChild.nodeValue=" Simulate"}drawPlaybackStop(){window.stopPlayback=!0}}var Nt=`
</h1>
<p>Version: Mon May 27 2024
(0a5ef43)</p>
`;document.querySelector("#app").innerHTML=`
<main>
  <section id="viewer">
    <article id="viewer-settings-knob">
      <i class="fa-solid fa-bars"></i>
    </article>
    <article id="viewer-settings">
      <menu>
        <!-- <li>
          <button id="zoomIntoViewButton" class="buttonRightFlat">
            <i class="fa-solid fa-magnifying-glass-plus"></i>
          </button>
        </li>
        <li>
          <button id="zoomOutViewButton" class="buttonLeftFlat">
            <i class="fa-solid fa-magnifying-glass-minus"></i>
          </button>
        </li> -->
        <li>
          <button id="playViewButton">
            <i class="fa-solid fa-play"></i> Simulate
          </button>
        </li>
      </menu>
      <menu class="menu-right">
      <li>
        <button id="saveProjectMenuButton" class="buttonRightFlat">
          <i class="fa-solid fa-floppy-disk"></i> Save
        </button>
      </li>
      <li>
        <button id="loadProjectMenuButton" class="buttonLeftFlat">
          <i class="fa-solid fa-folder-open"></i> Load
        </button>
      </li>
      <li>
        <button id="exportProjectMenuButton">
          <i class="fa-solid fa-file-export"></i> Export
        </button>
      </li>
        <li>
          <button id="openSettingsButton">
            <i class="fa-solid fa-gear"></i> Settings
          </button>
        </li>
      </menu>
    </article>
    <article id="viewer-main">
      <canvas class="zdog-canvas"></canvas>
    </article>
  </section>
  <section id="tools">
    <article id="tools-edit">
      <menu>
        <li>
          <button id="addLineEditButton">
            <i class="fa-solid fa-lines-leaning"></i> New line
          </button>
        </li>
        <li>
          <button id="addArcEditButton">
            <i class="fa-solid fa-bezier-curve"></i> New arc
          </button>
        </li>
      </menu>
    </article>
    <article id="tools-list">
      <ul id="list-commands"></ul>
    </article>
    <article id="tools-list-buttons">
      <menu>
        <!-- <li>
          <button id="editCommandButton" disabled>
            <i class="fa-solid fa-pen-to-square"></i> Edit
          </button>
        </li> -->
        <li>
          <button id="removeCommandButton">
            <i class="fa-solid fa-trash"></i> Remove
          </button>
        </li>
      </menu>
      <menu class="menu-impressum">
        <li>
          <a href="#" id="helpLink">About Tailorbird</a>
        </li>
      </menu>
    </article>
  </section>
</main>

<!-- Dialogs -->
<dialog id="helpDialog">
  <img src="./icon.svg" width="75" height="75" id="helpLogo">
  <h1>Tailorbird 3D ${Nt}
  <h2><i class="fa-solid fa-keyboard"></i> Controls</h2>
  <p>
   <ul>
    <li><i class="fa-solid fa-magnifying-glass"></i> <span class="helpControlText">Zoom: Scroll wheel</span></li>
    <li><i class="fa-solid fa-left-right"></i> <span class="helpControlText">Rotate horizontally: Drag</span></li>
    <li><i class="fa-solid fa-up-down"></i> <span class="helpControlText">Rotate vertically: Shift+Drag</span></li>
    <li><i class="fa-solid fa-up-down-left-right"></i> <span class="helpControlText">Move: Opt/Alt+Drag</span></li>
    <li><i class="fa-solid fa-share fa-rotate-90"></i> <span class="helpControlText">Jump to: O</span></li>
   </ul>
  </p>
  <h2><i class="fa-solid fa-address-card"></i></i> Help</h2>
  <p>
  <ul>
    <li>
      <a href="mailto:jakob.yanagibashi@rwth-aachen.de"><i class="fa-solid fa-envelope"></i> <span class="helpControlText">Email</span></a>
    </li>
    <li>
      <a href="https://www.kg.rwth-aachen.de/cms/KG/Footer/Service/~vcyq/Impressum/"><i class="fa-solid fa-circle-info"></i> <span class="helpControlText">Impressum</span></a>
    </li>
    </ul>
  </p>
  <form method="dialog">
    <menu>
      <li><button value="example1">Load example 1</button></li>
      <li><button value="example2">Load example 2</button></li>
      <li><button value="cancel">Close</button></li>
    </menu>
  </form>
</dialog>

<!-- Dialogs Main menu -->
<dialog id="saveDialog">
  <form method="dialog">
    <input type="text" id="saveDialogInput" />
    <menu>
      <button value="save">Save</button>
      <button value="cancel">Cancel</button>
    </menu>
  </form>
</dialog>

<dialog id="loadDialog">
  <form method="dialog">
    <input type="text" id="loadDialogInput" />
    <br />
    <ul id="loadDialogList">
    </ul>
    <menu>
      <button value="load">Load</button>
      <button value="cancel">Cancel</button>
    </menu>
  </form>
</dialog>

<dialog id="exportDialog">
  <form method="dialog">
    <menu>
      <button value="export">Save as G-code</button>
      <button value="close">Close</button>
    </menu>
  </form>
</dialog>

<!-- Dialogs Edit menu -->
<dialog id="addLineDialog">
  <form id="addLineForm">
    <h3>New line</h3>
    <fieldset>
      <legend>Position</legend>
      <input
        type="radio"
        id="addLineInputAbsRel2"
        name="addLineInputAbsRel"
        value="rel"
        checked
      />
      <label for="addLineInputAbsRel2">Relative</label>
      <input
        type="radio"
        id="addLineInputAbsRel1"
        name="addLineInputAbsRel"
        value="abs"
      />
      <label for="addLineInputAbsRel1">Absolute</label>
      <br />
      X: <input type="text" id="addLineInputX" value="0" /><br />
      Y: <input type="text" id="addLineInputY" value="0" /><br />
      Z: <input type="text" id="addLineInputZ" value="0" /><br />
    </fieldset>
    <fieldset>
      <legend>Behavior</legend>
      <input
        type="radio"
        id="addLineInputExtPmmAbs1"
        name="addLineInputExtPmmAbs"
        value="pmm"
        checked
      />
      <label for="addLineInputExtPmmAbs1">per mm</label>
      <input
        type="radio"
        id="addLineInputExtPmmAbs2"
        name="addLineInputExtPmmAbs"
        value="abs"
      />
      <label for="addLineInputExtPmmAbs2">Absolute</label>
      <br />
      Flow (<strong>E</strong>xtrusion):
      <input
        type="text"
        id="addLineInputE"
        value="1"
      /><br />
      Speed (<strong>F</strong>eed):
      <input
        type="text"
        id="addLineInputF"
        value="1500"
      /><br />
    </fieldset>
    <menu>
      <button id="addLineBtn" value="add">Add</button>
      <button value="cancel" formmethod="dialog">Cancel</button>
    </menu>
  </form>
</dialog>

<dialog id="addArcDialog">
  <form method="dialog" id="addArcForm">
    <h3>New arc</h3>
    <fieldset>
      <legend>Properties</legend>
      Size: <input type="text" id="addArcInputSize" value="5" /><br />
      Curvature:
      <br />
      <input
        type="radio"
        id="addArcInputCurvature1"
        name="addArcInputCurvature"
        value="1"
        checked
      />
      <label for="addArcInputCurvature1">Curved upwards</label>
      <input
        type="radio"
        id="addArcInputCurvature2"
        name="addArcInputCurvature"
        value="2"
      />
      <label for="addArcInputCurvature2">Curved downwards</label>
      <br />
      Direction:
      <br />
      <input
        type="radio"
        id="addArcInputDirection1"
        name="addArcInputDirection"
        value="1"
        checked
      />
      <label for="addArcInputDirection1">X pos</label>
      <input
        type="radio"
        id="addArcInputDirection2"
        name="addArcInputDirection"
        value="2"
      />
      <label for="addArcInputDirection2">Y pos</label>
      <input
        type="radio"
        id="addArcInputDirection3"
        name="addArcInputDirection"
        value="3"
      />
      <label for="addArcInputDirection3">X neg</label>
      <input
        type="radio"
        id="addArcInputDirection4"
        name="addArcInputDirection"
        value="4"
      />
      <label for="addArcInputDirection4">Y neg</label>

      <br />
      <input
        type="radio"
        id="addArcInputDirectionZ1"
        name="addArcInputDirectionZ"
        value="1"
        checked
      />
      <label for="addArcInputDirectionZ1">Up</label>

      <input
        type="radio"
        id="addArcInputDirectionZ2"
        name="addArcInputDirectionZ"
        value="2"
      />
      <label for="addArcInputDirectionZ2">Down</label>

      <input
        type="radio"
        id="addArcInputDirectionZ3"
        name="addArcInputDirectionZ"
        value="3"
      />
      <label for="addArcInputDirectionZ2">Flat</label>
    </fieldset>

    <fieldset>
      <legend>Behavior</legend>
      Flow:
      <input
        type="text"
        id="addArcInputE"
        placeholder="Extrusion rate"
        value="0"
      /><br />
      Speed:
      <input
        type="text"
        id="addArcInputF"
        placeholder="Feed rate"
        value="1500"
      /><br />
    </fieldset>
    <menu>
      <button value="add">Add</button>
      <button value="cancel">Cancel</button>
    </menu>
  </form>
</dialog>

<!-- Dialogs View menu -->
<dialog id="settingsDialog">
  <form method="dialog" id="settingsForm">
    <fieldset>
      <legend>Grid</legend>
      <input
        type="checkbox"
        id="settingsGridDisplay"
        name="settingsGridDisplay"
        checked=""
      />
      <label for="settingsGridDisplay"> Display grid</label><br />
      <br />
      <label for="settingsGridSizeInput">Size: </label>
      <input
        type="text"
        id="settingsGridSizeInput"
        placeholder="Default: 10"
      />
      mm
    </fieldset>
    <fieldset>
      <legend>Rendering</legend>
      <input
        type="checkbox"
        id="settingsRenderNoExtrusionDisplay"
        name="settingsRenderNoExtrusionDisplay"
        checked=""
      />
      <label for="settingsRenderNoExtrusionDisplay">
        Display moves without extrusion</label
      >
    </fieldset>
    <fieldset>
      <legend>G-code Export</legend>
      <label for="settingsGcodeStartCode">Start Code</label><br />
      <textarea id="settingsGcodeStartCode" name="settingsGcodeStartCode">
; -- START GCODE --
G90
M82
G28
G92 E0.0000
G1 E-4.0000 F60000
; -- end of START GCODE --</textarea
      >
      <br /><br />
      <label for="settingsGcodeEndCode">End Code</label><br />
      <textarea id="settingsGcodeEndCode" name="settingsGcodeEndCode">
; -- END GCODE --
G92 E0.0000
G1 E-4.0000 F60000
G28
; -- end of END GCODE --</textarea
      >
    </fieldset>
    <menu>
      <button value="apply">Apply</button>
    </menu>
  </form>
</dialog>
`;function jt(){var d=new k,n=10,t=30;const r=20,i=1,s=3;for(var u={x:0,y:0,e:-t},e={x:0,y:0,e:0},l=0;l<r;l++){for(var a=0;a<n;a++){e.x=Math.sin(1/n*2*Math.PI*a)*t,e.y=Math.cos(1/n*2*Math.PI*a)*t,e.e=u.e+Math.hypot(e.x-u.x,e.y-u.y);var o=new D(e.e*i,1250,e.x,e.y,l*s+s/n);d.model.append(o),u.x=e.x,u.y=e.y,u.e=e.e}l<r/2?(t+=.5+l%10/10,n+=1):(t-=.5+l%10/10,n-=1)}return d}function qt(){var d=new k,n=4,t=0;const r=35;var i;d.addLine(t,2500,0,0,0),d.addLine(t,1500,r-n,0,0),d.addLine(t+=4,1500,r,0,0),d.addLine(t,1500,r+15,0,0),d.addLine(t,1500,r-n,0,10);for(var s=1;s<10;s++)i=s*n,d.addLine(t,2e3,r-i,0,0),d.addLine(t+=n,1500,r-i,0,0),d.addArc(t+=i*1.8,1500,i,"1","1","1"),d.addLine(t,2e3,r+i/2,0,Math.max(i-25,0)),d.addLine(t,2e3,r+i/2+10,0,0),d.addLine(t,1500,r,0,i+10),d.addLine(t,1500,r-i-10,0,i);return d}function Vt(){window.selectedListEntries.forEach(d=>{d.listElement.parentNode.removeChild(d.listElement),window.currentProject.model.remove(d)}),window.selectedListEntries=[]}function Xt(){window.illo=new w.Illustration({element:".zdog-canvas",scale:10,resize:!0,rotate:{x:w.TAU*.2}}),window.illo.element.addEventListener("wheel",d=>{d.preventDefault(),!(window.illo.scale.x<2&&d.deltaY>0||window.illo.scale.x>80&&d.deltaY<0)&&window.illo.scale.multiply(1-Math.max(Math.min(d.deltaY/40,.9),-.9))}),window.viewRotation=new w.Vector,new w.Dragger({startElement:window.illo.element,onDragStart:function(d){d.buttons==4||d.altKey?(window.dragStartRX=window.illo.translate.x,window.dragStartRY=window.illo.translate.y):d.shiftKey?window.dragStartRX=window.illo.rotate.x:window.dragStartRX=window.illo.rotate.z},onDragMove:function(d,n,t){if(d.buttons==4||d.altKey)window.illo.translate.x=window.dragStartRX+n,window.illo.translate.y=window.dragStartRY+t;else if(d.shiftKey){let r=t/illo.width*w.TAU*-1;window.illo.rotate.x=window.dragStartRX+r}else{let r=n/illo.width*w.TAU*-1;window.illo.rotate.z=window.dragStartRX+r}}}),window.addEventListener("keydown",d=>{if(d.isComposing||d.key!=="o"||!window.selectedListEntries[0])return;const n=window.selectedListEntries[0];if(!(!1 in n||!1 in n)){var t=new w.Vector({x:-n.goaly.value*window.illo.scale.x,y:-n.goalx.value*window.illo.scale.y,z:-n.goalz.value*window.illo.scale.z});t.rotate(window.illo.rotate),console.log("jump to X: "+t.x+", Y: "+t.y),window.illo.translate=t}}),window.currentProject.renderAll(),Ct()}function Ct(){window.illo.updateRenderGraph(),window.animationReq=requestAnimationFrame(Ct)}function Yt(d){console.log("Save project to: "+d),Kt(window.currentProject,d)}function Ut(d){console.log("Load project from: "+d);const n=Wt(d);window.currentProject=n}function Wt(d){const n=localStorage.getItem("model"+d);if(!n)return;const t=JSON.parse(n);var r=new k;return t.moves.forEach(i=>{r.model.append(new D(i[0].value,i[1].value,i[2].value,i[3].value,i[4].value))}),r}function Kt(d,n){var t={moves:[]};currentProject.model.commands.forEach(i=>{t.moves.push([i.flow,i.speed,i.goalx,i.goaly,i.goalz])});const r=JSON.stringify(t);console.log(r),localStorage.setItem("model"+n,r)}function Ht(){_t(),Jt(),Qt(),Zt()}function _t(){document.getElementById("exportProjectMenuButton").addEventListener("click",function(){document.getElementById("exportDialog").showModal()}),document.getElementById("saveProjectMenuButton").addEventListener("click",function(){document.getElementById("saveDialog").showModal()}),document.getElementById("loadProjectMenuButton").addEventListener("click",function(){const n=document.getElementById("loadDialogList");n.replaceChildren();for(let r=0;r<localStorage.length;r++)if(localStorage.key(r).startsWith("model")){var t=document.createElement("li");t.appendChild(document.createTextNode(localStorage.key(r).substring(5))),n.appendChild(t)}document.getElementById("loadDialog").showModal()}),document.getElementById("helpLink").addEventListener("click",function(){document.getElementById("helpDialog").showModal()})}function Jt(){document.getElementById("addLineEditButton").addEventListener("click",function(){document.getElementById("addLineDialog").showModal()}),document.getElementById("addArcEditButton").addEventListener("click",function(){document.getElementById("addArcDialog").showModal()}),document.getElementById("removeCommandButton").addEventListener("click",Vt),document.getElementById("list-commands").addEventListener("click",function(d){d.preventDefault(),d.shiftKey||(window.selectedListEntries.forEach(n=>n.removeSelection()),window.selectedListEntries=[]),d.target&&d.target.matches("li")&&(d.target.command.addSelection(),window.selectedListEntries.push(d.target.command))})}function Qt(){document.getElementById("viewer-settings").addEventListener("click",function(d){d.eventPhase===2&&(d.offsetX>50||(this.style.visibility="hidden",this.style.opacity="0",document.getElementById("viewer-settings-knob").style.display="block"))}),document.getElementById("viewer-settings-knob").addEventListener("click",function(d){document.getElementById("viewer-settings").style.visibility="visible",document.getElementById("viewer-settings").style.opacity="1",this.style.display="none"}),document.getElementById("playViewButton").addEventListener("click",function(d){d.target.lastChild.nodeValue.includes("Simulate")?window.currentProject.drawPlaybackStart():window.currentProject.drawPlaybackStop()}),document.getElementById("openSettingsButton").addEventListener("click",function(d){document.getElementById("settingsDialog").showModal()})}function $t(d,n){const t=document.createElementNS("http://www.w3.org/1999/xhtml","a");t.download=n,t.rel="noopener",t.href=URL.createObjectURL(d),t.target="_blank",setTimeout(()=>{URL.revokeObjectURL(t.href),document.body.removeChild(t)},3e4),setTimeout(()=>{document.body.appendChild(t),t.click()},0)}function Zt(){document.getElementById("exportDialog").addEventListener("close",function(){if(event.target.returnValue!="export")return;console.log("Exporting to G-code...");var n="";let t={start:null,end:null};(t.start=localStorage.getItem("settingsGcodeStartCode"))&&(t.end=localStorage.getItem("settingsGcodeEndCode"))||(t={start:`; -- START GCODE --
G90
M82
G28
G92 E0.0000
G1 E-4.0000 F60000
; -- end of START GCODE --
`,end:`; -- END GCODE --
G92 E0.0000
G1 E-4.0000 F60000
G28
; -- end of END GCODE --
`}),n+=t.start,currentProject.model.commands.forEach(i=>{n+=i+`
`}),n+=t.end;var r=new Blob([n],{type:"text/plain;charset=utf-8"});$t(r,"tailorbird.gcode")}),document.getElementById("settingsDialog").addEventListener("close",function(n){if(n.target.returnValue!="apply")return;const t=new FormData(document.querySelector("#settingsForm"));t.has("settingsGridDisplay")?localStorage.setItem("settingsGridDisplay","on"):localStorage.setItem("settingsGridDisplay","off");const r=document.getElementById("settingsGridSizeInput");Number.isInteger(parseInt(r.value,10))?localStorage.setItem("settingsGridSize",r.value):localStorage.setItem("settingsGridSize","10"),r.value=localStorage.getItem("settingsGridSize"),t.has("settingsRenderNoExtrusionDisplay")?localStorage.setItem("settingsRenderNoExtrusionDisplay","on"):localStorage.setItem("settingsRenderNoExtrusionDisplay","off"),localStorage.setItem("settingsGcodeStartCode",t.get("settingsGcodeStartCode")),localStorage.setItem("settingsGcodeEndCode",t.get("settingsGcodeEndCode")),window.currentProject.renderAll()}),document.getElementById("saveDialog").addEventListener("close",function(n){if(n.target.returnValue!="save")return;const t=document.getElementById("saveDialogInput");Yt(t.value)}),document.getElementById("loadDialog").addEventListener("close",function(n){if(n.target.returnValue!="load")return;const t=document.getElementById("loadDialogInput"),r=document.getElementById("saveDialogInput");Ut(t.value),r.value=t.value,t.value=""}),document.getElementById("helpDialog").addEventListener("close",function(n){switch(n.target.returnValue){case"example1":window.currentProject=jt();break;case"example2":window.currentProject=qt();break;default:return}}),document.getElementById("addLineBtn").addEventListener("click",d=>{d.preventDefault();let n=document.getElementById("addLineInputX"),t=document.getElementById("addLineInputY"),r=document.getElementById("addLineInputZ"),i=document.getElementById("addLineInputE"),s=document.getElementById("addLineInputF");const u=new FormData(document.querySelector("#addLineForm")),e=currentProject.model.currentCoordinates();u.get("addLineInputAbsRel")=="rel"?u.get("addLineInputExtPmmAbs")=="pmm"?window.currentProject.addLine(Number.parseFloat(i.value)*Math.hypot(Number.parseFloat(n.value),Number.parseFloat(t.value),Number.parseFloat(r.value))+e.e,Number.parseFloat(s.value),Number.parseFloat(n.value)+e.x,Number.parseFloat(t.value)+e.y,Number.parseFloat(r.value)+e.z):window.currentProject.addLine(Number.parseFloat(i.value)+e.e,Number.parseFloat(s.value),Number.parseFloat(n.value)+e.x,Number.parseFloat(t.value)+e.y,Number.parseFloat(r.value)+e.z):u.get("addLineInputExtPmmAbs")=="pmm"?window.currentProject.addLine(Number.parseFloat(i.value)*Math.hypot(Number.parseFloat(n.value)-e.x,Number.parseFloat(t.value)-e.y,Number.parseFloat(r.value)-e.z)+e.e,Number.parseFloat(s.value),Number.parseFloat(n.value),Number.parseFloat(t.value),Number.parseFloat(r.value)):window.currentProject.addLine(Number.parseFloat(i.value)+e.e,Number.parseFloat(s.value),Number.parseFloat(n.value),Number.parseFloat(t.value),Number.parseFloat(r.value)),n.value="0",t.value="0",r.value="0",document.getElementById("addLineDialog").close()}),document.getElementById("addArcDialog").addEventListener("close",function(n){if(n.target.returnValue!="add")return;const t=document.getElementById("addArcInputSize"),r=document.getElementById("addArcInputE"),i=document.getElementById("addArcInputF"),s=new FormData(document.querySelector("#addArcForm")),u=currentProject.model.currentCoordinates();window.currentProject.addArc(Number.parseFloat(r.value)+u.e,Number.parseFloat(i.value),t.value,s.get("addArcInputDirection"),s.get("addArcInputDirectionZ"),s.get("addArcInputCurvature")),t.value="5"})}window.onload=function(){window.currentProject=new k,window.selectedListEntries=[],Xt(),Ht(),localStorage.getItem("splashSeen")!=="1"&&(localStorage.setItem("splashSeen","1"),document.getElementById("helpDialog").showModal())};
